function Btn({className, value, funcEvent}){
    return (
        <button type = "button" className={className} onClick={(e)=>{funcEvent(value, e)}}>{value}</button>
    )
}

export default Btn