import { Component } from "react";
import Btn from "../btn/btn";
import Input from "../input/input";


import "./buy-list.css";

class BuyList extends Component {
    state = {
        showInput: false,
        currentItem: "",
        items: [],
        editItem: false
    }

    addItem = () => {
        if (!this.state.showInput) {
            this.setState((state) => {
                return {
                    ...state,
                    showInput: true
                }
            })
            document.querySelector('.buy-list__save').classList.remove('hide')
        } else {
            this.setState((state) => {
                return {
                    ...state,
                    showInput: false
                }
            })
            document.querySelector('.buy-list__save').classList.add('hide')
        }
    }

    setCurrentItem = () => {
        let itemValue = document.querySelector('.input-item').value;

        this.setState((state) => {
            return {
                ...state,
                currentItem: itemValue,
            }
        })
    }

    saveItem = () => {

        if (this.state.currentItem === '') {
            alert('Введіть назву продукта!')
        } else {
            this.setState((state) => {
                return {
                    ...state,
                    currentItem: '',
                    items: [...this.state.items, this.state.currentItem]
                }
            })
        }
        document.querySelector('.input-item').value = '';
    }

    operations = (value, e) => {
        let clickedItem = e.target.parentElement

        if (value === '✍️') {
            let editInput = clickedItem.childNodes[4].children[0];
            let editedItemIndex = clickedItem.attributes.data.textContent
            let editedItem = this.state.items[editedItemIndex]
            let editBlock = clickedItem.childNodes[4]
            let newItems = [];

            if (!this.state.editItem) {
                editInput.value = editedItem
                editBlock.classList.remove('hide')
                this.setState((state) => {
                    return {
                        ...state,
                        editItem: true
                    }
                })
            } else {
                editBlock.classList.add('hide')
                newItems = this.state.items.map((el, i) => {
                    if (i === parseInt(editedItemIndex)) {
                        return el = editInput.value
                    }
                    return el
                })
                this.setState((state) => {
                    return {
                        ...state,
                        editItem: false,
                        items: newItems
                    }
                })
            }
        } else if (value === "❌") {
            if(this.state.editItem){
                alert('Потрібно закінчити редагування! Натисніть на ✍️ біля товару, який редагуєте!')
            } else {
                let stateWithoutDeletedItem = this.state.items
                .filter((item) => item !== clickedItem.childNodes[0].textContent)
            this.setState((state) => {
                return {
                    ...state,
                    items: stateWithoutDeletedItem
                }
            })
            }
        } else if (value === "✅") {
            clickedItem.classList.toggle('done')
        }
    }

    render() {
        return (
            <div className="buy-list">
                <h2>Список покупок</h2>
                <div className="buy-list__add">
                    <Btn value="додати ➕" className="add-btn" funcEvent={this.addItem}></Btn>
                </div>
                <div className="buy-list__save hide">
                    <Input placeholder="Введіть назву продукту" funcEvent={this.setCurrentItem}></Input>
                    <Btn value="зберегти 📀" className="save-btn" funcEvent={this.saveItem}></Btn>
                </div>
                <ul className="product-list">
                    {
                        this.state.items.map((el, i) => {
                            return (
                                <li key={i} data={i} className="product-item">
                                    <span className="product-item__text">{el}</span>
                                    <Btn value="✍️" className="btn-edit" funcEvent={this.operations}></Btn>
                                    <Btn value="❌" className="btn-delete" funcEvent={this.operations}></Btn>
                                    <Btn value="✅" className="btn-confirm" funcEvent={this.operations}></Btn>
                                    <div className="edit-block hide">
                                        <input type="text" placeholder="Введіть змінену назву продукту" className="input-edit" id={`input-edit${i}`}></input>
                                    </div>
                                </li>
                            )
                        })
                    }
                </ul>
            </div>
        )
    }
}

export default BuyList
