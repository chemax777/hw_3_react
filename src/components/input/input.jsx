function Input({ placeholder, funcEvent}) {
    return (
        <input type="text" placeholder={placeholder} className="input-item" onInput={()=>{funcEvent()}}></input>
    )
}

export default Input